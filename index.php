<?php
session_start();
require_once 'framework/Runner.php';
defined('BASEPATH') OR exit('No direct script access allowed');

$requestURI = explode('/', $_SERVER['REQUEST_URI']);
$scriptName = explode('/',$_SERVER['SCRIPT_NAME']);

for ($i= 0; $i < sizeof($scriptName); $i++)
{
  if ($requestURI[$i] == $scriptName[$i])
  {
      unset($requestURI[$i]);
  }
}

$command = array_values($requestURI);

$action = "index";
// se explota la variable command en su posicion 0 para saver si esta el controlador dentro de un modulo
$modulo = explode('-',$command[0]);
// explotamos para obtener el modulo y su respectivo controlador por default
$separar_url = explode('/',$routes['default_controller']);
// se explota la variable separar_url en su posicion 0 para saver si esta el controlador por default dentro de un modulo
$modulo_routers = explode('-',$separar_url[0]);
// verificamos si existe m para saver si es un controlador o una carpeta de modulo
// en caso contraio se carga el controlador por defecto sin modulo alguno
if($modulo_routers[0]=='m'):
	$mod = $separar_url[0]. '/';
	$controller = $separar_url[1];
else:
	$mod = '';
	$controller = $routes['default_controller'];
endif;
// verificacion si existe variable command en su posicion 0
if($command[0] !='' )
	// verificamos si existe m para saver si es un controlador o una carpeta de modulo
	// en caso contraio se carga el controlador por defecto sin modulo alguno
	if($modulo[0]=='m'){
		// verificacion si el controlador existe para poder llenarlo y luego mandarlo a llamar
		if(isset($command[1]) && trim($command[1])){
			$mod = $command[0]. '/';
			$controller = $command[1];
		}
		// verificacion si abra que realizar una accion en el respectivo controlador
		if(isset($command[2]) && trim($command[1])){
			$action = $command[2];
		}
	}
	else
	{
		// verificacion si el controlador existe para poder llenarlo y luego mandarlo a llamar
		if(isset($command[0]) && trim($command[0])){
			$mod = '';
			$controller = $command[0];
		}
		// verificacion si abra que realizar una accion en el respectivo controlador
		if(isset($command[1])){
			$action = $command[1];
		}
	}
	$action = str_replace("-", "_", $action);

$controllerPath = CONTROLLERS_PATH . '/' . $mod . $controller . ".php";
if(!file_exists($controllerPath))
{
	error_404();
	return;
}

require_once $controllerPath;

# Create a instance of controller

$className = ucfirst(strtolower($controller)) . "Controller";

$instance = new $className;
if(!method_exists($instance, $action))
{
	error_404();
	return;
}
$instance->$action();

?>
