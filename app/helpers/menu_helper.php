<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MenuHelper
{
	public function GetMenu()
	{
		return self::Menu;
	}

	const Menu = array(

		"Inicio" => array(
			"icon" => 'fas fa-home',
			'page' => 'events'
		),

		"Perfil" => array(
			"icon" => 'fas fa-user-circle',
			'page' => 'users'
		),

		"Mis Iniciativas" => array(
			"icon" => 'fas fa-id-card',
			'page' => 'events/myInitiatives'
		),



	);
}