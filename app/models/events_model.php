<?php
defined('BASEPATH') or exit('No direct script access allowed');
class EventsModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM events WHERE deletes = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":event_id" => $Id);
        $query = "SELECT * FROM events WHERE event_id = :event_id AND deletes = 0";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }

    public function get_user_id($Id)
    {
        $params = array(":user_id" => $Id);
        $query = "SELECT * FROM events WHERE user_id = :user_id AND deletes = 0 ";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetchAll();
    }

    public function save($data)
    {
        try {
            $query = "INSERT INTO events(user_id,name,description,due_date) VALUES(:user_id,:name,:description,:due_date)";
            $q = $this->default->prepare($query);
            $q->execute($data);
            $id = $this->default->lastInsertId();
            return "0{+}" . $id;
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
        return 1;
    }
    public function update($data)
    {
        try {
            $query = "UPDATE events SET user_id = :user_id,name = :name,description = :description,likes = :likes,due_date = :due_date,deleted_at = :deleted_at,reason_delete = :reason_delete,delete = :delete WHERE event_id = :event_id";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
        return 1;
    }

    public function remove($data)
    {
        try {
            $query = "UPDATE events SET reason_delete = :reason_delete, deletes = :delete WHERE event_id = :event_id";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
        return 1;
    }
}