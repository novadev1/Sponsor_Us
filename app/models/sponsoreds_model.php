<?php
defined('BASEPATH') or exit('No direct script access allowed');
class SponsoredsModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM sponsoreds";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }

    public function get_sponsor_id($Id)
    {
        $params = array(":need_id" => $Id);
        $query = "SELECT users.avatar, users.name, users.email, users.phone, sponsoreds.description, sponsoreds.sponsored_id, sponsoreds.need_id, sponsoreds.sponsored_by  FROM sponsoreds
        INNER JOIN users ON sponsoreds.user_id = users.user_id WHERE need_id = :need_id order by sponsoreds.sponsored_by asc";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetchAll();
    }

    public function get_sponsor_id_lis($Id)
    {
        $params = array(":need_id" => $Id);
        $query = "SELECT users.avatar, users.name, users.email, users.phone, sponsoreds.description, sponsoreds.sponsored_id, sponsoreds.need_id, sponsoreds.sponsored_by  FROM sponsoreds
        INNER JOIN users ON sponsoreds.user_id = users.user_id WHERE need_id = :need_id  AND sponsoreds.sponsored_by = 1 order by sponsoreds.sponsored_by asc";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetchAll();
    }

    public function get_sponsor_id_lis_in($Id)
    {
        $params = array(":need_id" => $Id);
        $query = "SELECT users.avatar, users.name, users.email, users.phone, sponsoreds.description, sponsoreds.sponsored_id, sponsoreds.need_id, sponsoreds.sponsored_by  FROM sponsoreds
        INNER JOIN users ON sponsoreds.user_id = users.user_id WHERE need_id = :need_id  AND sponsoreds.sponsored_by = 1 order by sponsoreds.sponsored_by asc";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetchAll();
    }

    public function get_id($Id)
    {
        $params = array(":sponsored_id" => $Id);
        $query = "SELECT * FROM sponsoreds WHERE sponsored_id = :sponsored_id";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try {
            $query = "INSERT INTO sponsoreds(need_id,user_id,description) VALUES(:need_id,:user_id,:description)";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
        return 1;
    }
    public function update($data,$id)
    {
        try {
            $query = "UPDATE sponsoreds SET sponsored_by = :sponsored_by WHERE sponsored_id = :sponsored_id";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return self::update_masivo($id);
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
        return 1;
    }

    public function update_masivo($data)
    {
        try {
            $params = array(":id" => $data);
            $query = "UPDATE sponsoreds SET sponsored_by = 2 WHERE need_id = :id AND sponsored_by = 0";
            $q = $this->default->prepare($query);
            $q->execute($params);
            return 0;
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
        return 1;
    }

    public function remove($data)
    {
        try {
            $q = $this->default->prepare('DELETE FROM sponsoreds WHERE sponsored_id= :sponsored_id');
            $q->execute($data);
            return 0;
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
        return 1;
    }
}
