<?php
defined('BASEPATH') or exit('No direct script access allowed');
class NeedsModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM needs";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":need_id" => $Id);
        $query = "SELECT * FROM needs WHERE need_id = :need_id";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }

    public function get_event_id($Id)
    {
        $params = array(":event_id" => $Id);
        $query = "SELECT * FROM needs WHERE event_id = :event_id";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetchAll();
    }
    public function save($data)
    {
        try
        {
            $query = "INSERT INTO needs(event_id,name,description) VALUES(:event_id,:name,:description)";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
        return 1;
    }

    public function remove($data)
    {
        try
        {
            $q = $this->default->prepare('DELETE FROM needs WHERE need_id= :need_id');
            $q->execute($data);
            return 0;
        } catch (PDOException $e) {
            echo 'Error ' . $e->getMessage();
        }
        return 1;
    }
}
