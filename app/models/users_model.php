<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UsersModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM users";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":user_id" => $Id);
        $query = "SELECT * FROM users WHERE user_id = :user_id";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }

    public function get_pass($data)
    {
        $query = "SELECT * FROM users WHERE password = :password AND user_id = :user_id";
        $result = $this->default->prepare($query);
        $result->execute($data);
        return $result->fetchAll();
    }

    public function login($data)
    {
        $query = "SELECT * FROM users WHERE email = :email AND password = :password";
        $result = $this->default->prepare($query);
        $result->execute($data);
        return $result->fetchAll();
    }
    
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO users(name,email, password) VALUES(:name,:email, :password)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    public function update($name,$phone,$email,$avatar,$user_id)
    {
        try 
        {
            $query = "UPDATE users SET name = :name, phone = :phone, email = :email, avatar = :avatar WHERE user_id = :user_id";
            $q = $this->default->prepare($query);
            $q->execute(array(
                    ':name' => $name,
                    ':phone' => $phone,
                    ':email' => $email,
                    ':avatar' => $avatar,
                    ':user_id' => $user_id
                ));
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update_pass($data)
    {
        try 
        {
            $query = "UPDATE users SET password = :password WHERE user_id = :user_id";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function save_images($Id,$ext)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE users SET avatar = :avatar WHERE user_id = :user_id');
            $q->execute(array(":avatar" => (md5($Id).".".$ext), ":user_id" => $Id));
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('DELETE FROM users WHERE user_id= :user_id');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>