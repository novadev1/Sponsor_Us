<div class="col-md-12 mb-4">
    <div class="row">
        <div class="card mx-auto" style="width: 75rem">
            <div class="card-header">
                <?= $event['name'] ?> <span class="float-right">Vencimiento: <?= $event['due_date'] ?></span>
            </div>
            <div class="card-body">
                <p class="card-text"><?= $event['description'] ?></p>
                <hr>
                <img style="width: 25px" src="<?= base_url() ?>assets/img/like.png" alt=""> <span>325 likes</span> <span
                    class="float-right">36% patrocinado</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card mx-auto mt-5" style="width: 75rem; background-color: gray; color: white">
            <div class="card-header">
                <b><?= $need['name'] ?></b>
            </div>
            <div class="card-body">
                <p class="card-text"><?= $need['description'] ?></p>
            </div>
        </div>
        <div class="row float-left ml-5 mt-3 datos-ofr">
            <p>Cómo te gustaría patrocinarnos?</p>
        </div>
        <input type="hidden" class="event_id" value="<?= $need['event_id'] ?>">
        <div class="row float-left ml-5 mt-3 datos-ofr">
            <form datos="description" method='POST' role='form' class='description' data-target="<?= site_url() ?>"
                enctype='multipart/form-data'>
                <div class="card-body">
                    <div class="row">
                        <div class='col-md-12'>
                            <div class='form-group'>
                                <textarea name="description" id="description"
                                    placeholder="Cómo nos apoyarás?. Se generoso" class="form-control" required>
                                </textarea>
                            </div>
                        </div>
                        <input type="hidden" name="need_id" value="<?= $need['need_id'] ?>">
                        <a class="btn btn-success ofert" href="<?= base_url() ?>/sponsoreds/save">Ofertar patrosinio</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="row add-datos-ofr mt-4" style="margin-left: 20rem">

        </div>
    </div>
</div>