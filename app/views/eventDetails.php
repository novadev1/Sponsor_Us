<div class="col-md-12 mb-4">
    <div class="row">
        <div class="card mx-auto" style="width: 75rem">
            <div class="card-header">
                <?= $event['name'] ?> <span class="float-right">Vencimiento: <?= $event['due_date'] ?></span>
            </div>
            <div class="card-body">
                <p class="card-text"><?= $event['description'] ?></p>
                <hr>
                <img style="width: 25px" src="<?= base_url() ?>assets/img/like.png" alt=""> <span>325 likes</span> <span class="float-right">36% patrocinado</span>
            </div>
        </div>
    </div>
    <?php foreach ($needs as $need) : ?>
        <div class="row">
            <div class="card mt-5 mx-auto" style="width: 30rem;">
                <div style="background-color: gray" class="card-body">
                    <h5 class="card-title"><?= $need['name'] ?></h5>
                    <p class="card-text"><?= $need['description'] ?></p>
                </div>
                <hr>
                <?php 
                /*foreach ($sponsors as $row) {
                    if ($need['need_id'] == $row['']) {
                        # code...
                    } else {
                        # code...
                    }
                    
                }*/
                ?>
                <div class="card-body">
                    <?php if ($event['user_id'] == $_SESSION['Id']) : ?>
                        <?php if (count($sponsors) < 1) : ?>
                            <a href="<?= base_url() ?>events/offertList/<?= $event['event_id'] ?>/<?= $need['need_id'] ?>" class="btn btn-outline-success">Ver mis ofertas</a>
                        <?php else : ?>
                            <?php foreach ($sponsors as $sp) : ?>
                                <p><b>Patrocinado por:</b></p>
                                <h5 class="card-title"><img src="<?= base_url() ?>assets/img/<?= $sp['avatar'] ?>" class="img-thumbnail" style="width: 3rem"> <?= $sp['name'] ?></h5>
                                <hr>
                                <span class="float-left"><?= $sp['description'] ?><br> <?= $sp['phone'] ?> </span>
                            <?php endforeach ?>
                        <?php endif; ?>
                    <?php else : ?>
                        <?php if (count($sponsors) < 1) : ?>
                            <a href="<?= base_url() ?>events/needDetail/<?= $event['event_id'] ?>/<?= $need['need_id'] ?>" class="btn btn-outline-success">Ofertar
                                patrocinio</a>
                        <?php else : ?>
                            <?php foreach ($sponsors as $sp) : ?>
                                <p><b>Patrocinado por:</b></p>
                                <h5 class="card-title"><img src="<?= base_url() ?>assets/img/<?= $sp['avatar'] ?>" class="img-thumbnail" style="width: 3rem"> <?= $sp['name'] ?></h5>
                                <hr>
                                <span class="float-left"><?= $sp['description'] ?><br> <?= $sp['phone'] ?> </span>
                            <?php endforeach ?>
                        <?php endif; ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>