<div class="row">
    <div class="col-md-12">
        <label for="" class="col-md-12">
            <input class="form-control search-buscar" placeholder="Buscar" type="text">
        </label>
    </div>
    <div class="col-md-12">
        <div class="col-md-12">
            <a href="<?= base_url() ?>events/addmyinitiatives" class="btn btn-info float-right">Nueva iniciativa <i
                    class="fas fa-plus"></i></a>
        </div>
    </div>
</div>
<div class="col-md-12">
    <?php foreach ($initiatives as $row) : ?>
    <div class="card mb-4 mx-auto" style="width: 50rem;">
        <div class="card-body">
            <h5 class="card-title"><?= $row['name'] ?> <span class="float-right"><a href="#" data-target="<?= site_url('events/modal/'.$row['event_id']); ?>" class="btn btn-danger delete"><i class='fas fa-trash-alt'></i></a></span></h5>
            <p class="card-text"><?= $row['description'] ?></p>
            <a href="#" class="card-link"><i class="fas fa-heart"></i></a>
            <span class="float-right"><b>33% sponsor</b></span>
        </div>
        <div class="col-md-12 bg-dark">
            <a href="<?= base_url(); ?>events/eventDetails/<?= $row['event_id'] ?>"  class="card-link">Más detalles</a>
        </div>
    </div>
    <?php endforeach ?>
</div>
<div class="modal fade arv" id="modal_global" tabindex="-1" role="dialog" aria-labelledby="modal_global" aria-hidden="true">
</div>