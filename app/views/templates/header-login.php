<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php if(isset($title)) echo $title ?></title>
    <!-- Bootstrap core CSS-->
    <link href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/vendor/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- suvir img -->
    <link href="<?= base_url(); ?>assets/vendor/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="<?= base_url(); ?>assets/css/sb-admin.css" rel="stylesheet">
    <!-- sweetalert2 -->
    <link href="<?= base_url(); ?>assets/vendor/sweetalert2/css/sweetalert2.min.css" rel="stylesheet">
    <!-- datetimepicker -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/build/css/bootstrap-datetimepicker.min.css">
    <!-- pace animacion de carga -->
    <link href="<?= base_url(); ?>assets/vendor/pace/css/índice.css" rel="stylesheet">
    <!-- Css requires for specific view -->
    <?php  if(isset($css)) foreach ($css as $file) : echo $file; endforeach; ?>
  </head>
  <body id="page-top" class="sidebar-toggled">
