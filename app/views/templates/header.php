<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php if(isset($title)) echo $title ?></title>
    <!-- Bootstrap core CSS-->
    <link href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/vendor/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- suvir img -->
    <link href="<?= base_url(); ?>assets/vendor/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="<?= base_url(); ?>assets/css/sb-admin.css" rel="stylesheet">
    <!-- sweetalert2 -->
    <link href="<?= base_url(); ?>assets/vendor/sweetalert2/css/sweetalert2.min.css" rel="stylesheet">
    <!-- datetimepicker -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/build/css/bootstrap-datetimepicker.min.css">
    <!-- pace animacion de carga -->
    <link href="<?= base_url(); ?>assets/vendor/pace/css/índice.css" rel="stylesheet">
    <!-- Css requires for specific view -->
    <?php  if(isset($css)) foreach ($css as $file) : echo $file; endforeach; ?>
  </head>
  <body id="page-top" class="sidebar-toggled">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
      <a class="navbar-brand mr-1" href="index.html">Sponsor Us</a>
      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>
      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
          <a href="<?= site_url('login/logout') ?>">Salir</a>
      </form>
    </nav>
