      </div>
      <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendor/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- suvir img  -->
    <script src="<?= base_url(); ?>assets/vendor/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/vendor/bootstrap-fileinput/js/locales/es.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/vendor/bootstrap-fileinput/themes/fas/theme.js" type="text/javascript"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url(); ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <!-- Temporales -->
    <!-- <script src="< ?= base_url(); ?>assets/vendor/chart.js/Chart.min.js"></script> -->
    <!-- Custom scripts for all pages-->
    <script src="<?= base_url(); ?>assets/js/sb-admin.min.js"></script>
    <!-- Demo scripts for this page-->
    <!-- Temporales -->
    <!-- <script src="< ?= base_url(); ?>assets/js/demo/chart-area-demo.js"></script> -->
    <!-- Sweet Alert -->
    <script type="text/javascript" src="<?= base_url(); ?>assets/vendor/sweetalert2/js/sweetalert2.all.js"></script>
    <!-- Bootstrap Notify -->
    <script src="<?= base_url(); ?>assets/vendor/bootstrap/js/bootstrap-notify.min.js"></script>
    <!-- moment -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
    <!-- datetimepicker -->
    <script src="<?= base_url(); ?>assets/vendor/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Kendo UI -->
    <script src='<?= base_url(); ?>assets/vendor/kendo/js/kendo.all.min.js'></script>
    <script src='<?= base_url(); ?>assets/vendor/kendo/js/messages/kendo.messages.es-MX.min.js'></script>
    <!-- pace animacion de carga -->
    <script src='<?= base_url(); ?>assets/vendor/pace/js/pace.min.js'></script>

    <script src="<?= base_url(); ?>assets/vendor/mask/js/jquery.mask.min.js"></script>
    <!-- Scripts requires for specific view -->
    <?php if (isset($js)) {
    foreach ($js as $file) {
        echo $file;
    }
} ?>
  </body>
</html>
