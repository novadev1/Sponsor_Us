<div class="col-md-12 mt-5">
    <div class="card mx-auto" style="width: 18rem;">
        <form datos="login" method='POST' role='form' class='login' data-target="<?= site_url()  ?>"
            enctype='multipart/form-data'>
            <div class="card-body">
                <div class="row">
                    <div class='col-md-12'>
                        <div class='form-group'>
                            <label for='email'>Email</label>
                            <input type='email' id='email' name='email' class='form-control'
                                value="<?= (isset($data['email'])) ? $data['email'] : '' ?>" placeholder=''
                                data-target-ms='El campo email es obligatorio' required>
                        </div>
                        <div class='form-group'>
                            <label for='password'>Password</label>
                            <input type='password' id='password' name='password' class='form-control'
                                value="<?= (isset($data['password'])) ? $data['password'] : '' ?>" placeholder=''
                                data-target-ms='El campo password es obligatorio' required>
                        </div>
                        <a class="btn btn-success login-k" href="#">Ingresar</a>
                        <br>
                        <hr>
                        <a class="btn btn-info" href="<?= base_url() ?>login/register">Crear cuenta</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>