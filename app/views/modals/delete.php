<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">users</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card mx-auto" style="width: 45rem">
                <div class="card-header">
                    <?= $event['name'] ?> <span class="float-right">Vencimiento: <?= $event['due_date'] ?></span>
                </div>
                <div class="card-body">
                    <p class="card-text"><?= $event['description'] ?></p>
                    <hr>
                    <img style="width: 25px" src="<?= base_url() ?>assets/img/like.png" alt=""> <span>325 likes</span>
                    <span class="float-right">36% patrocinado</span>
                </div>
            </div>
            <span><b>Porque eliminara el evento</b></span>
            <textarea name="datos" class="form-control datos"></textarea>
            <input type="hidden" class="id" value="<?= $event['event_id'] ?>">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <a href="#" datos="<?= base_url() ?>events/remove" datoss="<?= base_url() ?>events"
                class="btn btn-danger eliminar">Eliminar registro</a>
        </div>
    </div>
</div>
<script src='<?= base_url(); ?>assets/modulosjs/main.js'></script>
<script>
$(".eliminar").click(function(event) {
    event.preventDefault();
    input = $(this);
    $modal = $("#modal_global");
    console.log(input.attr("datos"));

    $.ajax({
            type: "get",
            dataType: "html",
            dataType: 'json',
            url: input.attr("datos") + "/" + $(".datos").val() + "/" + $(".id").val()
        })
        .done(function(data) {
            if (data.val == 1) {
                mostrar_alert(data.ms);
                setTimeout(
                    "location.href='" +
                    input.attr("datoss") +
                    "/myInitiatives'",
                    500
                );
            } else {
                mostrar_alert(data.ms);
            }
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
});
</script>