<div class="col-md-12">
    <form datos="users" method='POST' role='form' class='addmyinitiatives' data-target="<?=site_url()?>"
        enctype='multipart/form-data'>
        <div class="card mb-4 mx-auto" style="width: 25rem;">
            <div class="card-body">
                <div class="row">
                    <div class='col-md-12'>
                        <div class='form-group'>
                            <label for='actividad'>Nombre de actividad</label>
                            <input type='text' id='actividad' name='name' class='form-control' placeholder='actividad'
                                required>
                        </div>
                        <div class='form-group'>
                            <label for='Descripcion'>Descripcion</label>
                            <textarea name="description" id="Descripcion" placeholder="Descripcion" class="form-control"
                                required></textarea>
                        </div>
                        <div class='form-group'>
                            <label for='fecha'>Fecha de vencimiento</label>
                            <input type='date' id='fecha' name='fecha' class='form-control' required>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mx-auto" style="width: 25rem;">
            <div class="add-card">
                <div class="card mt-4 mx-auto card1" style="width: 20rem;">
                    <div class="card-body">
                        <div class="row">
                            <div class='col-md-12'>
                                <div class='form-group'>
                                    <label for='actividad'>Nombre de Recurso</label>
                                    <input type='text' id='actividad' name='recurso[]' class='form-control'
                                        placeholder='actividad' required>
                                </div>
                                <div class='form-group'>
                                    <label for='Descripcion'>proviciones del recurso</label>
                                    <textarea name="Descripcion[]" id="Descripcion"
                                        placeholder='proviciones del recurso' class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <a class="btn btn-success btn-sm float-right add-recurso" href=""><i class="fas fa-plus-circle"></i></a>
            </div>
            <div class="col-md-12 mt-4 mb-4 d-flex justify-content-between align-items-center">
                <a class="btn btn-primary btn-sm float-right save" href="#">Guardar</a>
            </div>
        </div>
    </form>
</div>