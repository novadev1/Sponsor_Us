<div class="col-md-12 mb-4">
    <div class="card mx-auto" style="width: 18rem;">
        <form datos="users" method='POST' role='form' class='shangePerfil' data-target="<?= site_url()  ?>" enctype='multipart/form-data'>
            <?php $img = ($data['avatar'] != '') ? $data['avatar'] : 'default-profile.png' ; ?>
            <img src="<?= base_url();?>assets/img/<?= $img ?>" class="card-img-top img-prf"><br>
            <input type="hidden" name="respaldo_img" value="<?= $img ?>">
            <input id="prf" type="file" name="images" style="display: none;"/> 
            <label for="prf" style="margin-top: -10rem; margin-left: 15rem"> 
                 <span class="btn btn-success btn-round"> <i class="fas fa-plus-circle"></i></span> 
            </label>
            <div class="card-body">
                <div class="row">
                    <div class='col-md-12'> 
                        <div class='form-group'>
                            <label for='name'>Nombre</label>
                            <input type='text' id='name' name='name' class='form-control' value="<?= (isset($data['name']))? $data['name'] : '' ?>"  placeholder='' data-target-ms='El campo name es obligatorio' required>
                        </div>
                        <div class='form-group'>
                            <label for='phone'>Telefono</label>
                            <input type='text' id='phone' name='phone' class='form-control Telefono' value="<?= (isset($data['phone']))? $data['phone'] : '' ?>"  placeholder='' data-target-ms='El campo phone es obligatorio' required>
                        </div>
                        <div class='form-group'>
                            <label for='email'>Email</label>
                            <input type='text' id='email' name='email' class='form-control' value="<?= (isset($data['email']))? $data['email'] : '' ?>"  placeholder='' data-target-ms='El campo email es obligatorio' required>
                        </div>
                    </div>
                </div>
                <a class="btn btn-primary update" href="#">Actualizar</a>
                <input type="hidden" name="Id" value="<?= $valor = (isset($data['user_id']))? $data['user_id'] : '0' ?>">
            </div>
        </form>
    </div>
</div>
<div class="col-md-12">
    <div class="card mx-auto" style="width: 18rem;">
        <div class="card-body">
            <form datos="users" method='POST' role='form' class='shangePassword' data-target="<?= site_url()  ?>" enctype='multipart/form-data'>
                <div class="row"> 
                    <div class="col-md-12">
                        <div class='form-group'>
                            <label for='password'>Password anterior</label>
                            <input type='password' id='currentPassword' name='currentPassword' class='form-control' placeholder='Digite su antiguo paswword' required>
                        </div>
                        <div class='form-group'>
                            <label for='password'>Nuevo password</label>
                            <input type='password' id='Newpassword' name='Newpassword' class='form-control' placeholder='Digite un password nuevo' required>
                        </div>
                        <div class='form-group'>
                            <label for='password'>Repetir password</label>
                            <input type='password' id='RepeatPassword' name='passwRepeatPasswordord' class='form-control' placeholder='repita su password' required>
                        </div>   
                        <a class="btn btn-primary mod" href="#">Modificar password</a>
                    </div>             
                </div>
                <input type="hidden" name="Id" value="<?= $valor = (isset($data['user_id']))? $data['user_id'] : '0' ?>">
            </form>
        </div>
    </div>
</div>
