<div class="container">

    <div class="col-md-12">
        <div class="row">
            <div class="card mx-auto" style="width: 75rem">
                <div class="card-header">
                    <?= $event['name'] ?> <span class="float-right">Vencimiento: <?= $event['due_date'] ?></span>
                </div>
                <div class="card-body">
                    <p class="card-text"><?= $event['description'] ?></p>
                    <hr>
                    <img style="width: 25px" src="<?= base_url() ?>assets/img/like.png" alt=""> <span>325 likes</span>
                    <span class="float-right">36% patrocinado</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card mx-auto mt-5" style="width: 75rem; background-color: gray; color: white">
                <div class="card-header">
                    <b><?= $need['name'] ?></b>
                </div>
                <div class="card-body">
                    <p class="card-text"><?= $need['description'] ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <?php if (count($sponsors) < 1) : ?>
            <div></div>
            <p>No hay ofertas todavia</p>
            <?php endif; ?>
            <?php foreach ($sponsors as $row) : ?>
            <div class="card mb-4 mx-auto mt-4" style="width: 50rem;">
                <div class="card-body">
                    <h5 class="card-title"><img src="<?= base_url() ?>assets/img/<?= $row['avatar'] ?>"
                            class="img-thumbnail" style="width: 3rem"> <?= $row['name'] ?></h5>
                    <hr>
                    <p class="card-text"><?= $row['description'] ?></p>
                    <hr>
                    <span class="float-left"><?= $row['email'] ?><br> <?= $row['phone'] ?> </span>
                </div>
                <div class="col-md-12 mb-2">
                    <?php if ($row['sponsored_by'] == 1) : ?>
                    <span class="btn btn-info float-right disabled">Aceptada</span>
                    <?php elseif ($row['sponsored_by'] == 2) : ?>
                    <span class="btn btn-danger float-right">Rechasada</span>
                    <?php else : ?>
                    <a href="<?= base_url(); ?>events/aceptofert/<?= $row['sponsored_id'] ?>/<?= $row['need_id'] ?>/<?= $event['event_id'] ?>"
                        class="btn btn-success float-right">Aceptar oferta</a>
                    <?php endif ?>
                </div>
            </div>
            <?php endforeach ?>
        </div>
    </div>

</div>