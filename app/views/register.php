<div class="col-md-12 mt-5">
    <div class="card mx-auto" style="width: 18rem;">
    <form datos="register" method='POST' role='form' class='register' data-target="<?= site_url()  ?>" enctype='multipart/form-data'>
        <div class="card-body">
                <div class="row">
                    <div class='col-md-12'> 
                        <div class='form-group'>
                            <label for='name'>Nombre</label>
                            <input type='text' id='name' name='name' class='form-control' placeholder='' data-target-ms='El campo name es obligatorio' required>
                        </div>
                         <div class='form-group'>
                            <label for='email'>Email</label>
                            <input type='text' id='email' name='email' class='form-control'  placeholder='' data-target-ms='El campo email es obligatorio' required>
                        </div>
                        <div class='form-group'>
                            <label for='password'>Password</label>
                             <input type='password' id='password' name='password' class='form-control' placeholder='' data-target-ms='El campo password es obligatorio' required>
						</div>
                        <div class='form-group'>
                            <label for='password'>Repetir password</label>
                            <input type='password' id='RepeatPassword' name='passwRepeatPasswordord' class='form-control' placeholder='' data-target-ms='Este campo es obligatorio' required>
                        </div>
                        <a class="btn btn-success reg" href="#">Registrarme</a>
                    </div>
                </div>
        </div>
    </form>
    </div>
</div>