<?php
defined('BASEPATH') or exit('No direct script access allowed');
# URL Config

$config["base_url"] = "http://localhost/sponsor/";
$config['index_page'] = "";

# Database Configuration
$config['db'] = array(
    "default" => array(
        "dbname" => "sponsor",
        "username" => "root",
        "password" => "",
        "port" => "3306",
        "host" => "localhost",
    ),
);