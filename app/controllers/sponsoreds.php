<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!isset($_SESSION['Id'])) {
    header("Location: ".base_url()."login");
}
class SponsoredsController extends Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('sponsoreds_model', 'model');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","sponsoreds"),
            $this->library->modulosjs("main"));
        $data['css'] = array(   
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "sponsoreds";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "sponsoreds";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->model->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(3);
        if ($Id > 0 ) {
            $data['data'] = $this->model->get_id($Id);
            $this->load->view("modals/sponsoreds",$data);
        }else{
            $this->load->view("modals/sponsoreds");
        }
    }
    public function save()
    {
        $info = array(
            ':need_id' => strtoupper(trim($this->input->post("need_id"))),
			':user_id' => strtoupper(trim($this->input->post("user_id"))),
			':description' => strtoupper(trim($this->input->post("description"))),
			':sponsored_by' => strtoupper(trim($this->input->post("sponsored_by")))
        );
        $save = $this->model->save($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':need_id' => strtoupper(trim($this->input->post("need_id"))),
			':user_id' => strtoupper(trim($this->input->post("user_id"))),
			':description' => strtoupper(trim($this->input->post("description"))),
			':sponsored_by' => strtoupper(trim($this->input->post("sponsored_by"))),
            ':sponsored_id' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->model->update($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    
    public function remove()
    {
        $info = array(
            ':sponsored_id' => strtoupper(trim($this->uri->segment(3)))
        );
        $save = $this->model->remove($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
}
