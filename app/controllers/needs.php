<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!isset($_SESSION['Id'])) {
    header("Location: ".base_url()."login");
}
class NeedsController extends Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('needs_model', 'model');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","needs"),
            $this->library->modulosjs("main"));
        $data['css'] = array(   
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "needs";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "needs";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function save()
    {
        $info = array(
            ':event_id' => strtoupper(trim($this->input->post("event_id"))),
			':name' => strtoupper(trim($this->input->post("name"))),
			':description' => strtoupper(trim($this->input->post("description"))),
			':sponsored_quantity' => strtoupper(trim($this->input->post("sponsored_quantity")))
        );
        $save = $this->model->save($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':event_id' => strtoupper(trim($this->input->post("event_id"))),
			':name' => strtoupper(trim($this->input->post("name"))),
			':description' => strtoupper(trim($this->input->post("description"))),
			':sponsored_quantity' => strtoupper(trim($this->input->post("sponsored_quantity"))),
            ':need_id' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->model->update($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    
    public function remove()
    {
        $info = array(
            ':need_id' => strtoupper(trim($this->uri->segment(3)))
        );
        $save = $this->model->remove($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
}
?>