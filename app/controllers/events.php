<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (!isset($_SESSION['Id'])) {
    header("Location: " . base_url() . "login");
}
class EventsController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('events_model', 'model');
        $this->load->model('needs_model', 'needs');
        $this->load->model('needs_model', 'needs');
        $this->load->model('sponsoreds_model', 'sponsors');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data['initiatives'] = $this->model->get_consulta($_SESSION['Id']);
        // $data['sponsors'] = $this->sponsors->get_sponsor_id_lis_in($id2);
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('initiatives', $data);
        $this->load->view('templates/footer', $data);
    }

    public function myInitiatives()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader", "events"),
            $this->library->modulosjs("main")
        );
        $data['initiatives'] = $this->model->get_user_id($_SESSION['Id']);
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('myInitiatives', $data);
        $this->load->view('templates/footer', $data);
    }

    public function addmyinitiatives()
    {

        $data["js"] = array(
            $this->library->modulosjs("loader", "events"),
            $this->library->modulosjs("main")
        );
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('addmyinitiatives', $data);
        $this->load->view('templates/footer', $data);
    }

    public function eventDetails()
    {
        $id = $this->uri->segment(3);
        $id2 = $this->uri->segment(4);
        $data['event'] = $this->model->get_id($id);

        $data['needs'] = $this->needs->get_event_id($id);

        $data['sponsors'] = $this->sponsors->get_sponsor_id_lis($id2);
        //$data['sponsors'] = $this->sponsors->get_consulta();

        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('eventDetails', $data);
        $this->load->view('templates/footer', $data);
    }

    public function needDetail()
    {

        $data["js"] = array(
            $this->library->modulosjs("loader", "needs"),
            $this->library->modulosjs("main")
        );

        $event_id = $this->uri->segment(3);
        $need_id = $this->uri->segment(4);
        $data['event'] = $this->model->get_id($event_id);

        $data['need'] = $this->needs->get_id($need_id);

        $data['sponsors'] = $this->sponsors->get_sponsor_id($need_id);

        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('sendOffertToNeed', $data);
        $this->load->view('templates/footer', $data);
    }

    public function offertList()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader", "needs"),
            $this->library->modulosjs("main")
        );

        $event_id = $this->uri->segment(3);
        $need_id = $this->uri->segment(4);
        $data['event'] = $this->model->get_id($event_id);

        $data['need'] = $this->needs->get_id($need_id);

        $data['sponsors'] = $this->sponsors->get_sponsor_id($need_id);
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('offertList', $data);
        $this->load->view('templates/footer', $data);
    }

    public function save()
    {
        $actividad = array(
            ':user_id' => $_SESSION['Id'],
            ':name' => strtoupper($this->input->post("name")),
            ':description' => strtoupper($this->input->post("description")),
            ':due_date' => strtoupper($this->input->post("fecha")),
        );
        $save = $this->model->save($actividad);
        $save = explode("{+}", $save);
        if ($save[0] != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        } else {
            $recurso = $this->input->post("recurso");
            $description = $this->input->post("Descripcion");
            $res = 1;
            for ($i = 0; $i < count($recurso); $i++) {
                $recursos = array(
                    ':event_id' => $save[1],
                    ':name' => strtoupper($recurso[$i]),
                    ':description' => $description[$i],
                );
                $res = $this->needs->save($recursos);
            }
            if ($res != 0) {
                $datos = "Error,Ocurrio un error!,danger,top,center";
                echo json_encode($datos);
            } else {
                $datos = "success,Se agrego con exito!,success,top,center";
                echo json_encode($datos);
            }
        }
    }

    public function add_offert()
    {
        $actividad = array(
            ':need_id' => $this->input->post("need_id"),
            ':user_id' => $_SESSION['Id'],
            ':description' => strtoupper($this->input->post("description")),
        );
        $save = $this->sponsors->save($actividad);
        if ($save != 0) {
            $datosss = array(
                'val' => false,
                'ms' => "Error,Credenciales incorrectas!,danger,top,center"
            );
            echo json_encode($datosss);
        } else {
            $datosss = array(
                'val' => true,
                'ms' => "success,Se realizo una oferta con exito!,success,top,center"
            );
            echo json_encode($datosss);
        }
    }

    public function aceptofert()
    {
        $id = $this->uri->segment(3);
        $need_id = $this->uri->segment(4);
        $event_id = $this->uri->segment(5);
        $actividad = array(
            ':sponsored_by' => 1,
            ':sponsored_id' => $id
        );
        $save = $this->sponsors->update($actividad, $need_id);
        if ($save != 0) {
            $datosss = array(
                'val' => false,
                'ms' => "Error,Credenciales incorrectas!,danger,top,center"
            );
            echo json_encode($datosss);
        } else {
            header("Location: " . base_url() . "events/eventDetails/" . $event_id);
        }
    }

    public function modal()
    {
        $Id = $this->uri->segment(3);
        $data['event'] = $this->model->get_id($Id);
        $this->load->view("modals/delete", $data);
    }

    public function remove()
    {
        $info = array(
            ':reason_delete' => strtoupper(trim($this->uri->segment(3))),
            ':delete' => 1,
            ':event_id' => strtoupper(trim($this->uri->segment(4)))
        );
        $save = $this->model->remove($info);
        if ($save != 0) {
            $datosss = array(
                'val' => false,
                'ms' => "Error,Credenciales incorrectas!,danger,top,center"
            );
            echo json_encode($datosss);
        } else {
            $datosss = array(
                'val' => true,
                'ms' => "success,Se realizo una oferta con exito!,success,top,center"
            );
            echo json_encode($datosss);
        }
    }
}