<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!isset($_SESSION['Id'])) {
    header("Location: ".base_url()."login");
}
class UsersController extends Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('users_model', 'model');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
        /*
         * Settings for Upload Library.
         *
         */
        $config['expensions'] = array("jpeg", "jpg", "png");
        $config['target_directory'] = BASEPATH . "/assets/img/";
        /*****************************************************************/
        $this->load->library('upload');
        $this->upload->set_config($config);
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","users"),
            $this->library->modulosjs("main"));
        $data['data'] = $this->model->get_id($_SESSION['Id']);
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('profile', $data);
        $this->load->view('templates/footer', $data);
    }

    public function profile()
    {
        $Id = $this->uri->segment(3);
        if (empty($id)) {
            $data['data'] = $this->model->get_id($Id);
        }
        
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('sponsor', $data);
        $this->load->view('templates/footer', $data);
    }


    public function all()
    {
        $data = $this->model->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(3);
        if ($Id > 0 ) {
            $data['data'] = $this->model->get_id($Id);
            $this->load->view("modals/users",$data);
        }else{
            $this->load->view("modals/users");
        }
    }
    public function save()
    {
        $info = array(
            ':name' => strtoupper(trim($this->input->post("name"))),
			':email' => strtoupper(trim($this->input->post("email"))),
            ':password' => strtoupper(trim($this->input->post("password")))
        );
        $save = $this->model->save($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $upload_image = "";
        if (!empty($_FILES['images']['name'])) {
            $temp = explode('.',$_FILES['images']['name']);
            $upload_image = $this->input->post("respaldo_img"); 
        }else{
            $upload_image = 'perfil.jpg';
        }
        $save = $this->model->update($this->input->post("name"),$this->input->post("phone"),$this->input->post("email"),$upload_image,$this->input->post("Id"));
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            if (!empty($_FILES['images']['name'])) {
                $upload = $this->upload->upload_images(md5($this->input->post("Id")));
                $save = $this->model->save_images($this->input->post("Id"),$upload['extension']);
            }
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

    public function update_pass()
    {
        $datos = array(
            ":password"=>$this->input->post("currentPassword"),
            ":user_id"=>$this->input->post("Id")
        );
        $comp = $this->model->get_pass($datos);
        if (count($comp) > 0) {
            $info = array(
                ':password' => trim($this->input->post("Newpassword")),
                ':user_id' => trim($this->input->post("Id"))
            );

            $save = $this->model->update_pass($info);
            if ($save != 0) {
                $datos = "Error,Ocurrio un error!,danger,top,center";
                echo json_encode($datos);
            }else{
                $datos = "success,Se edito un registro con exito!,success,top,center";
                echo json_encode($datos);
            }
        }else{
            $datos = "Error,No conside el password anterior!,danger,top,center";
            echo json_encode($datos);
        }            
    }
    
    public function remove()
    {
        $info = array(
            ':user_id' => strtoupper(trim($this->uri->segment(3)))
        );
        $save = $this->model->remove($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
}
?>