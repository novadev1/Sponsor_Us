<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('users_model', 'model');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader", "login"),
            $this->library->modulosjs("main")
        );
        $this->load->view('templates/header-login',$data);
        $this->load->view('login');
        $this->load->view('templates/footer-login',$data);
    }

    public function register()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader", "login"),
            $this->library->modulosjs("main")
        );
        $this->load->view('templates/header-login');
        $this->load->view('register');
        $this->load->view('templates/footer-login',$data);
        
    }

   public function login(){
        $info = array(
        ':email' => trim($this->input->post("email")),
        ':password' => trim($this->input->post("password")),
        );
        $save = $this->model->login($info);
        //print_r($save);
        if (count($save) == 0) {
        $datosss = array(
            'val' => false,
            'ms' => "Error,Credenciales incorrectas!,danger,top,center" 
        );
        echo json_encode($datosss);
        }else{
        $_SESSION['Id'] = $save[0][0];
        $datosss = array(
            'val' => true,
            'ms' => "success,bienvenido!,success,top,center" 
        );
        echo json_encode($datosss);
        }
   }

   public function logout()
   {
        session_destroy();
        header("Location: ".base_url()."login");
   }
   public function userRegister(){
    $info = array(
        ':name' => trim($this->input->post("name")),
        ':email' => trim($this->input->post("email")),
        ':password' => trim($this->input->post("password")),
    );
    $save = $this->model->save($info);
    if ($save != 0) {
        $datosss = array(
            'val' => false,
            'ms' => "Error,Ocurrio un error!,danger,top,center" 
        );
        echo json_encode($datosss);
    }else{
        $_SESSION['Id'] = $save[0][0];
        $datosss = array(
            'val' => true,
            'ms' => "success,usuario registrado con exito!,success,top,center" 
        );
        echo json_encode($datosss);
    }
   }
}
?>