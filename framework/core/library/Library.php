<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library {
	# Instancia única de la clase actual.
    private static $instance;

    # Se encarga de verificar si ya existe una instancia de esta clase,
    # si la clase se encuentra instanciada retornará dicha instancia caso
    # contrario creará una nueva instancia antes de retornarla.
    public static function GetInstance()
    {
        if(!isset(self::$instance))
        {
            $context = __CLASS__;
            self::$instance = new $context;
        }
        return self::$instance;
    }

    public function js($library_name, $mod="")
	{
		$library_name = strtolower($library_name);
        $contentFileFullPath = ASSETS_PATH . (($mod == "")? '' : '/'.$mod). "/" .$library_name . ".js";
        $url = base_url() . "assets".(($mod == "")? '' : '/'.$mod). "/" .$library_name . ".js";
        if (!file_exists($contentFileFullPath)) {
            error_404();
            return;
        }
        $path = "<script src='" . $contentFileFullPath . "'></script>";
        return $path;
	}
	public function css($library_name, $mod="")
	{
		$library_name = strtolower($library_name);
        $contentFileFullPath = ASSETS_PATH . (($mod == "")? '' : '/'.$mod). "/" . $library_name . ".css";
        $url = base_url() . "assets".(($mod == "")? '' : '/'.$mod). "/" .$library_name . ".css";
        if (!file_exists($contentFileFullPath)) {
            error_404();
            return;
        }
        $path = "<link rel='stylesheet' href='" . $contentFileFullPath . "'/>";
        return $path;
	}
	public function modulosjs($library_name, $mod1="", $mod2="")
	{
		$library_name = strtolower($library_name);
        $contentFileFullPath = ASSETS_PATH . "/modulosjs".(($mod1 == "")? '' : '/'.$mod1). "/" .(($mod2 == "")? '' : '/'.$mod2). "/" . $library_name . ".js";
        $url = base_url() . "assets/modulosjs".(($mod1 == "")? '' : '/'.$mod1). "/" .(($mod2 == "")? '' : '/'.$mod2). "/" .$library_name . ".js";
        if (!file_exists($contentFileFullPath)) {
            error_404();
            return;
        }
        $path = "<script src='" . $url . "'></script>";
        return $path;
	}
	public function vendorjs($library_name, $mod_1="",$mod_2="")
	{
		$library_name = strtolower($library_name);
        $contentFileFullPath = ASSETS_PATH . "/vendor".(($mod_1 == "")? '' : '/'.$mod_1).(($mod_2 == "")? '' : '/'.$mod_2). "/" .$library_name . ".js";
        $url = base_url() . "assets/vendor".(($mod_1 == "")? '' : '/'.$mod_1).(($mod_2 == "")? '' : '/'.$mod_2). "/" . $library_name . ".js";

        if (!file_exists($contentFileFullPath))
        {
            error_404();
            return;
        }
        $path = "<script src='" . $url . "'></script>";
        return $path;
	}

	public function vendorcss($library_name, $mod_1="",$mod_2="")
	{
		$library_name = strtolower($library_name);
        $contentFileFullPath = ASSETS_PATH . "/vendor".(($mod_1 == "")? '' : '/'.$mod_1).(($mod_2 == "")? '' : '/'.$mod_2). "/" .$library_name . ".css";
        $url = base_url() . "assets/vendor".(($mod_1 == "")? '' : '/'.$mod_1).(($mod_2 == "")? '' : '/'.$mod_2). "/" . $library_name . ".css";
        if (!file_exists($contentFileFullPath))
        {
            error_404();
            return;
        }
        $path = "<link rel='stylesheet' href='" . $url . "'/>";
        return $path;
	}
}
