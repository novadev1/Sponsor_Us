<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model 
{
    #
    # Objecto que almacena las conexiones a las diferentes bases de 
    # datos.
    #
	public $pdo;
    /**
     * Se crea la conexión al servidor dentro del contructor para que todas las clases que hereden
     * la clase Connection tengan acceso a la base de datos
     */
    public function __construct($required_servers = array()) 
    {	
    	global $config;
    	try 
    	{
    		foreach ($config['db'] as $server => $settings) 
    		{
    			if(!in_array($server, $required_servers)) continue;
    			$this->pdo[$server] = new PDO("mysql:host=" . $settings['host'] . "; dbname=" . $settings['dbname']. "; port=". $settings['port'], $settings['username'], 
                $settings['password']);
                $this->pdo[$server]->exec('SET CHARACTER SET utf8');
                $this->pdo[$server]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->pdo[$server]->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    		}
		}
		catch(PDOException $e)
		{
			echo 'Error!: '.$e->getMessage();
		}
    }

    public function __get($name)
    {
    	return $this->pdo[$name];
    }
}