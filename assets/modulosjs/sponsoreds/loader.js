
/* Al cargar la página se procede a cargar la información de los empleados */
$.ajax({  
    type: 'get',
    dataType: 'json', 
    url: $("#grid").attr('reload-data-target'),
    success:function(response){
        /* se le asigna la información al grid */
        var grid = $('#grid').getKendoGrid();
        grid.dataSource.data(response);
        grid.refresh();
    }
});

/* Se inicializa el grid y las columnas y formatos que contendrá
 * Ejemplo de uso:
 * https://demos.telerik.com/kendo-ui/grid/index
 */
$("#grid").kendoGrid({
    toolbar: [ 
        { template: kendo.template($("#template").html()) }
    ],
    dataSource: {
        type: "odata",
        transport: {
        },
        pageSize: 20,
        schema: {
            model: {
                id: 'sponsored_id',
                fields: {
                    sponsored_id : {
                        type: 'number',
                        editable: false,
                        nullable: false
                    },
                    need_id : {
					                        type: 'string',
					                        editable: false,
					                        nullable: false
					                    },user_id : {
					                        type: 'string',
					                        editable: false,
					                        nullable: false
					                    },description : {
					                        type: 'string',
					                        editable: false,
					                        nullable: false
					                    },sponsored_by : {
						                        type: 'string',
						                        editable: false,
						                        nullable: false
						                    }
                }
            }
        }
    },
    height: 700,
    groupable: false,
    sortable: true,
    pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5
    },
    columns: [        
    
													{
												        field: "need_id",
												        title: "need_id",
												        width: 100,
												        headerAttributes: { style: "color: #646464;" },
												        attributes: { style: "text-align: center"}
												    },
													{
												        field: "user_id",
												        title: "user_id",
												        width: 100,
												        headerAttributes: { style: "color: #646464;" },
												        attributes: { style: "text-align: center"}
												    },
													{
												        field: "description",
												        title: "description",
												        width: 100,
												        headerAttributes: { style: "color: #646464;" },
												        attributes: { style: "text-align: center"}
												    },
														{
													        field: "sponsored_by",
													        title: "sponsored_by",
													        width: 100,
													        headerAttributes: { style: "color: #646464;" },
													        attributes: { style: "text-align: center"}
													    },
    {
        title: "Opciones",
        template: "<a class='btn btn-primary' onclick='modals(#= sponsored_id  #)'  data-toggle='tooltip' data-placement='left' style='color: white' > <i class='far fa-edit'></i></a>&nbsp;&nbsp;&nbsp;<a class='btn btn-danger eliminar' onclick='eliminar(#= sponsored_id  #)'  data-toggle='tooltip' data-placement='left' style='color: white' > <i class='fas fa-trash-alt'></i></a>",
        width: 64
    }
    ],
    selectable: "row",
    filterable: true,
});
/*
 * 
 * Cuando se escribe dentro de la caja de búsqueda se captura el evento y lo
 * que el usuario ha escrito para realizar la busqueda dentro del grid.
 *
 *
 */
$("input.search-buscar").on('input', function()
{
    /* Se captura lo que el usuario ha escrito */
    var q = $(this).val();
    /* Obtiene el grid que contiene la información */
    var grid = $("#grid").data("kendoGrid");
    /* Se realiza la búsqueda dentro del grid 
     * Ejemplo de uso:
     * https://telerikhelper.net/2014/10/21/how-to-external-search-box-for-kendo-ui-grid/
     */
    grid.dataSource.query({
        page:1,
        pageSize:20,
        filter:{
            logic:"or", // "or" o "and"
            filters:[               
                {field:"Titulo", operator:"contains", value:q}, // se puede usar operator: 'eq', si se desea que la columna contenga exactamente el mismo valor que el usuario ingreso
                {field:"Descripcion", operator:"contains", value:q},
                {field:"Nombre", operator:"contains", value:q}
            ]
        }
    }); 
});
