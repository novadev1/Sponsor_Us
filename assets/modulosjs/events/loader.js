var numero = 1;
$(".add-recurso").click(function(event) {
  event.preventDefault();
  numero = numero + 1;
  $(".add-card").append(
    '<div class="card mt-4 mx-auto card' +
      numero +
      '" style="width: 20rem;">' +
      '<div class="card-body">' +
      '<div class="row">' +
      '<div class="col-md-12">' +
      '<a class="btn btn-danger btn-sm float-right del_card" onclick="del_card(' +
      numero +
      '); return false;" href=""><i class="fas fa-plus-circle"></i></a>' +
      '<div class="form-group">' +
      '<label for="actividad">Nombre de Recurso</label>' +
      '<input type="text" id="actividad" name="recurso[]" class="form-control" placeholder="actividad" required>' +
      "</div>" +
      '<div class="form-group">' +
      '<label for="Descripcion">proviciones del recurso</label>' +
      '<textarea name="Descripcion[]" id="Descripcion" placeholder="proviciones del recurso" class="form-control" required></textarea>' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>"
  );
});
function del_card(id) {
  $(".card" + id).remove();
}
$(".save").click(function(event) {
  event.preventDefault();
  $(".addmyinitiatives").submit();
});
$(".addmyinitiatives").submit(function(event) {
  //console.log('entro');
  event.preventDefault();
  var $form = $(this);

  //console.log($form.attr("data-target"));
  var $formData = new FormData(this);
  $.ajax({
    method: "post",
    dataType: "json",
    url: $form.attr("data-target") + "events/save",
    data: $formData,
    contentType: false,
    cache: false,
    processData: false
  })
    .done(function(data) {
      mostrar_alert(data);
      setTimeout(
        "location.href='" + $form.attr("data-target") + "events/myInitiatives'",
        500
      );
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
});
$(".delete").click(function(event) {
  event.preventDefault();
  input = $(this);
  $modal = $("#modal_global");
  $.ajax({
    type: "get",
    dataType: "html",
    url: input.attr("data-target")
  })
    .done(function(response) {
      $modal.html(response);
      $modal.modal("show");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
});
$(".eliminar").click(function(event) {
  event.preventDefault();
  input = $(this);
  $modal = $("#modal_global");
  $.ajax({
    type: "get",
    dataType: "html",
    url:
      input.attr("data-target") + "/" + $(".datos").val() + "/" + $(".id").val()
  })
    .done(function(data) {
      if (data.val == 1) {
        mostrar_alert(data.ms);
        setTimeout(
          "location.href='" + $form.attr("data-target") + "events'",
          500
        );
      } else {
        mostrar_alert(data.ms);
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
});
