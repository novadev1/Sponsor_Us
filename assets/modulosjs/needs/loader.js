$('.ofert').click(function(event) {
    event.preventDefault();
    $('.description').submit();   
});
$('.description').submit(function(event) {
    //console.log('entro');
    event.preventDefault();
    var $form = $(this);
    
    //console.log($form.attr("data-target"));
    var $formData = new FormData(this);
    $.ajax({
        method: 'post', 
        dataType: 'json',
        url: $form.attr("data-target") + "events/add_offert",
        data: $formData,
        contentType:false,
        cache:false,
        processData:false,
    })
    .done(function(data) {
        console.log(data.val);
        if (data.val == 1) {
            mostrar_alert(data.ms);
            event_id = $('.event_id').val();
            $(".datos-ofr").html('');
            $(".add-datos-ofr").html('<div class="col-md-12">'+
                '<center>'+
                    '<div class="col-md-12">'+
                        '<p class="mx-auto">Gracias por su oferta, los pondremos en contacto para afinar de talles</p>'+
                    '</div>'+
                    '<div class="col-md-12">'+
                        '<a href="'+$form.attr("data-target")+'events/eventDetails/'+event_id+'" class="btn btn-success" >Ok</a>'+
                    '</div>'+
                '</center>'+
            '</div>');
        } else {
            mostrar_alert(data.ms);

        }
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
    
});