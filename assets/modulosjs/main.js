function eliminar(datos) {
	$modal = $('#modal_global');
	swal
	({
	  title: '¿Seguro que desea eliminar este registro?',
	  text: "¡No podras revertir esto!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  cancelButtonText: 'Cancelar',
	  confirmButtonText: 'Si, ¡Deseo Borrarlo!'
	}).then((result) => {
		if (result.value) 
		{
			$.ajax({
				type: 'get',
				dataType: 'json',
				url: $modal.attr('data-target').replace("modal", "remove/") + datos
			})
			.done(function(data) {
				console.log("success");
				reloadData();
				mostrar_alert(data);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}
	});			
};
function mostrar_alert(datos) {
	var $datos = datos.split([',']);
	$.notify({
		title: '<strong>'+$datos[0]+'!</strong>',
		message: $datos[1]+'!'
	},{
		type: $datos[2],
		z_index: '10000',
		placement: {
	  		from: $datos[3],
	  		align: $datos[4]
		}
	});
}
function mostrar_alert_sw(datos) {
  var $datos = datos.split([',']);
  swal({
    position: $datos[0],
    type: $datos[1],
    title: $datos[2],
    showConfirmButton: false,
    timer: $datos[3]
  }); 
}
function validacion_campos() {
	var $error = false;
	$('.add input, .add select').each(function(index, el) {
		var $input = $(this);
		if ($input.attr('required')) {
			if ($input.val() === '' || $input.val() <= 0) {
				$ms = $(this).attr('data-target-ms');
				$datos = "Error,"+$ms+",danger,top,center";
      			/*mostrar_alert(datos);*/
      			$error = true;
      			$input.addClass('invalid');
      			if ($input.siblings('label.label').length == 0) {
      				$input.after('<label class="invalid label">'+$ms+'</label>');
      			}
			}else {
				$input.removeClass('invalid');
				$input.siblings('label.label').remove();				
			}
		}
	});
	return $error;
}
function validacion_tamano_numeros() {
	var $error = false;
	$('.add input, .add select').each(function(index, el) {
		var $input = $(this);
		if ($input.attr('required')) {
			if (String($input.val()).length < $input.attr('tamano') && $input.is('[tamano]')) {
				$ms = $(this).attr('data-target-ms-numero');
				$datos = "Error,"+$ms+",danger,top,center";
      			/*mostrar_alert(datos);*/
      			$error = true;
      			$input.addClass('invalid');
      			if ($input.siblings('label.label').length == 0) {
      				$input.after('<label class="invalid label">'+$ms+'</label>');
      			}
			}else {
				$input.removeClass('invalid');
				$input.siblings('label.label').remove();				
			}
		}
	});
	return $error;
}
//validaciones generales
$('.numero').keypress(function(e) {
  tecla = (document.all) ? e.keyCode : e.which;

  //Tecla de retroceso para borrar, siempre la permite
  if (tecla == 8 || tecla == 0) {
    return true;
  }

  // Patron de entrada, en este caso solo acepta numeros
  patron = /[0-9-.]/;
  tecla_final = String.fromCharCode(tecla);
  return patron.test(tecla_final);
});
$(".Telefono").mask("(000) 0000-0000");
$(".Dui").mask("00000000-0");
//fin validaciones generales