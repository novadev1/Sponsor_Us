$('.login-k').click(function(event) {
	event.preventDefault();
	$('.login').submit();	
});
$('.login').submit(function(event) {
	//console.log('entro');
	event.preventDefault();
	var $form = $(this);
	
	//console.log($form.attr("data-target"));
	var $formData = new FormData(this);
	$.ajax({
		method: 'post', 
		dataType: 'json',
		url: $form.attr("data-target") + "login/login",
		data: $formData,
	    contentType:false,
        cache:false,
        processData:false,
	})
	.done(function(data) {
		console.log(data.val);
		if (data.val == 1) {
			mostrar_alert(data.ms);
			setTimeout("location.href='"+$form.attr("data-target")+"events'", 500);
		} else {
			mostrar_alert(data.ms);

		}
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
});


$('.reg').click(function(event) {
	event.preventDefault();
	$('.register').submit();	
});

$('.register').submit(function(event) {
	//console.log('entro');
	event.preventDefault();
	var $form = $(this);
	if ($('#password').val() == $('#RepeatPassword').val()) {
		//console.log($form.attr("data-target"));
		var $formData = new FormData(this);
		$.ajax({
			method: 'post', 
			dataType: 'json',
			url: $form.attr("data-target") + "login/userRegister",
			data: $formData,
		    contentType:false,
	        cache:false,
	        processData:false,
		})
		.done(function(data) {
			if (data.val == 1) {
				mostrar_alert(data.ms);
				setTimeout("location.href='"+$form.attr("data-target")+"login'", 500);
			} else {
				mostrar_alert(data.ms);

			}
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	} else {
		mostrar_alert("Error,Los passwor no considen!,danger,top,center");
		$('#password').val('');
		$('#RepeatPassword').val('');
	}
	
});