$('.update').click(function(event) {
	event.preventDefault();
	$('.shangePerfil').submit();	
});
$('.shangePerfil').submit(function(event) {
	//console.log('entro');
	event.preventDefault();
	var $form = $(this);
	
	//console.log($form.attr("data-target"));
	var $formData = new FormData(this);
	$.ajax({
		method: 'post', 
		dataType: 'json',
		url: $form.attr("data-target") + "users/update",
		data: $formData,
	    contentType:false,
        cache:false,
        processData:false,
	})
	.done(function(data) {
		console.log("success");
		mostrar_alert(data);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
});

$('.mod').click(function(event) {
	event.preventDefault();
	$('.shangePassword').submit();	
});
$('.shangePassword').submit(function(event) {
	//console.log('entro');
	event.preventDefault();
	var $form = $(this);
	if ($('#Newpassword').val() == $('#RepeatPassword').val()) {
		//console.log($form.attr("data-target"));
		var $formData = new FormData(this);
		$.ajax({
			method: 'post', 
			dataType: 'json',
			url: $form.attr("data-target") + "users/update_pass",
			data: $formData,
		    contentType:false,
	        cache:false,
	        processData:false,
		})
		.done(function(data) {
			$('#Newpassword').val('');
			$('#currentPassword').val('');
			$('#RepeatPassword').val('');
			mostrar_alert(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	} else {
		mostrar_alert("Error,Los passwor no considen!,danger,top,center");
		$('#Newpassword').val('');
		$('#RepeatPassword').val('');
	}
	
});
function readFile() {
      for (var i = 0; i < this.files.length; i++) {
        if (this.files && this.files[i]) {      
            var FR= new FileReader();       
            FR.addEventListener("load", function(e) {
                $(".img-prf").attr("src",e.target.result);
            });         
            FR.readAsDataURL(this.files[i]);
          } 
      }    
    }
    document.getElementById("prf").addEventListener("change", readFile);